from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import DetailView, ListView

def morceau_detail(request,pk):
    return HttpResponse('OK')

from django.views.generic import DetailView
from .models import Morceau, Artiste

class MorceauDetailView(DetailView):
    model = Morceau
    template_name = 'musiques/morceau_detail.html'

class ArtisteDetailView(DetailView):
    model = Artiste
    template_name = 'musiques/artiste.html'

class MorceauListView(ListView):
    model = Morceau
    template_name = 'musiques/morceau_list.html'