from django.conf.urls import url, include
from . import views
from .views import MorceauDetailView, ArtisteDetailView, MorceauListView

app_name = 'musiques'
urlpatterns = [
    url(r'^(?P<pk>\d+)$', MorceauDetailView.as_view(), name='morceau-detail'),
    url(r'^detailArtiste/(?P<pk>\d+)$', ArtisteDetailView.as_view(), name='artiste-detail'),
    url(r'^listemorceau/', MorceauListView.as_view(), name='morceau-liste'),
    ]